# ExoGENI tools

The goal of these tools is to make integration with the ExoGENI platform
with third party scripts and controllers easier.

TODO: documentation, sorry.

A draft specification of the JSON format uses by these tools is available
here: https://sarnet.uvalight.net/docs/json-spec.html


## Tools

* `xo-rpc`: send requests to an ExoGENI controller
* `xo-request-xml`: generate a slice request from a JSON file
* `ndl2json`: convert NDL-OWL manifests into JSON


## RPC functions

* `show-resources` -- Show overview of available resources
* `resources-rdf` -- RDF of available resources
* `slices` -- List active slices
* `request <slice-name> [NDL-OWL file]`
* `modify <slice-name> [NDL-OWL file]`
* `await <slice-name>` -- Wait for a slice to boot up
* `reservation <slice-name>`
* `properties <slice-name> <reservation-id>`
* `rdf <slice-name>`
* `delete <slice-name>`
* `info <slice-name>`
* `ansible-inv <slice-name>`
* `known-hosts <slice-name>` -- Update SSH known hosts for slice
* `renew <slice-name> [hours]` -- Update lease time on slice


## Installing

Dependencies:

    sudo apt install python3-rdflib

To install, run:

    # Current user only
    python3 setup.py install --user

    # All users
    sudo python3 setup.py install


A directory called `~/.config/exogeni/` should exist which contains
`geni.pem` (your GENI portal certificate),
`geni-key.pem` (the private key of that certificate),
and one or more `ssh-NAME.pub` files (SSH public keys that will have root
access to the VMs).
The file `~/.config/exogeni/xo-request.ini` should contain a property called
`config.default\_site`.
Tip: create the `~/.config/exogeni/` directory and copy the default
`xo-request.ini` config file to it.


## Usage example

Requesting a slice:

    $ xo-request-xml resources/simple.json  | xo-rpc request simple-slice
    ret Here are the leases:
    Request id: c55deeaa-52af-46a8-992d-cbed8a8c35e0
    [   Slice UID: c55deeaa-52af-46a8-992d-cbed8a8c35e0 | Reservation UID: 127e7f0a-a57b-4968-9b41-7d746d64827b | Resource Type: uvanlvmsite.vm | Resource Units: 1 ]
    No errors reported
    err False

Waiting for a slice to boot:

    $ xo-rpc await simple-slice
    simple               Ticketed  Reservation 127e7f0a-a57b-4968-9b41-7d746d64827b (Slice simple-slice) is in state [Ticketed,Redeeming]

    simple               Active  Reservation 127e7f0a-a57b-4968-9b41-7d746d64827b (Slice simple-slice) is in state [Active,None] 0:01:06.034776
    simple-slice OK 0:01:06.035030
