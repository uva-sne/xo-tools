from distutils.core import setup

setup(name='exogeni-tools',
      version='0.2',
      description='ExoGENI tools',
      author='Ben de Graaff',
      author_email='ben@nx3d.org',
      url='https://sarnet.uvalight.net/ben/#exogeni-tools',
      packages=['exogeni'],
      scripts=['bin/xo-rpc', 'bin/xo-request-xml', 'bin/xo-modify-xml', 'bin/ndl2json'],
      data_files=[('share/exogeni/', ['xo-request.ini'])])
