
from .rdf import objects, _short_name, deconstruct

MGMT_IP = 'http://geni-orca.renci.org/owl/topology.owl#managementIP'
MGMT_PORT = 'http://geni-orca.renci.org/owl/topology.owl#managementPort'

AVAIL_LABEL_SET = 'http://geni-orca.renci.org/owl/layer.owl#availableLabelSet'


def slice_management_ips(rdfs):
    subjects = {}
    for subj, term, val in objects(rdfs):
        term = str(term)
        if term == MGMT_IP:
            subjects.setdefault(_short_name(subj), {})['ip'] = str(val)
        elif term == MGMT_PORT:
            subjects.setdefault(_short_name(subj), {})['port'] = str(val)
    return subjects


def _one(v):
    if v is None:
        return v
    elif len(v) != 1:
        raise ValueError(v)
    return v[0]


def _vlan(vs):
    v = _one(vs)
    if v.endswith('.0'):
        return v[:-2]
    return v


def controller_resources(rdfs):
    resources = {}
    subjects, subject_types = deconstruct(rdfs)
    stitch_ports = {}

    for s, vs in subjects.items():
        rt = vs.get('domain#hasResourceType', [])
        if not rt:
            continue
        if 'compute#VM' in vs.get('domain#hasResourceType', []):
            resources[s] = _one(vs.get('collections#size'))

    for k in subject_types.get('topology#Interface', []):
        s = subjects[k]
        for v in s.get('ethernet#availableVLANSet', []):
            stitch_ports.setdefault(v, []).append(k)

    for k in subject_types.get('collections#Collection', []):
        if '/available' not in k:
            continue
        s = subjects[k]
        r = resources[k] = {}  # 'size': _one(s.get('collections#size'))
        if k in stitch_ports:
            r['ports'] = stitch_ports[k]
        elems = r['available'] = []
        for e in s.get('collections#element', []):
            e = subjects[e]
            if 'layer#lowerBound' in e and 'layer#upperBound' in e:
                lower = _one(e['layer#lowerBound']).rsplit('/', 1)[-1]
                upper = _one(e['layer#upperBound']).rsplit('/', 1)[-1]
                elems.append(lower + '-' + upper)
            elif 'layer#label_ID' in e:
                elems.append(_vlan(e['layer#label_ID']))
            else:
                raise ValueError(e)
        elems.sort()

    return resources
