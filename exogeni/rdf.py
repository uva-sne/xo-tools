from xml.etree.ElementTree import fromstring

alias_simple_map = {
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'rdf#type',
    'http://www.w3.org/2000/01/rdf-schema#label': 'rdf#label',
}


def _short_name(n):
    """Turn RDF ref into a shorter name"""
    if n.startswith('http://geni-orca.renci.org/owl/'):
        p = n.split('#', 1)
        pt = p[0][31:]
        if pt.endswith('.owl') or pt.endswith('.rdf'):
            pt = pt[:-4]
        # (XXX) UUID?
        if len(pt) == 36 and pt.count('-') == 4:
            return '#'.join(p[1:])
        p[0] = pt
        pt = '#'.join(p)
        #for r in prenames:
        #    if pt.startswith(r[0]):
        #        pt = r[1] + pt[len(r[0]):]
        #        break
        return pt

    if n.startswith('http://www.w3.org/2002/07/owl#'):
        return n[26:]

    return n


def shorten_name(v):
    v = str(v)
    if v.startswith('http://geni-orca.renci.org/owl/'):
        v = v[31:].split('#', 1)
        if v[0].endswith('.owl'):
            v[0] = v[0][:-4]
        return '#'.join(v)
    elif v.startswith('http://www.w3.org/2002/07/owl#'):
        v = v.split('#', 1)
        return 'owl#' + v[1]
    return alias_simple_map.get(v, v)


class URI(str):
    pass


RDF_DESC = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description'
RDF_ABOUT = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about'
RDF_NODE_ID = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}nodeID'
RDF_RESOURCE = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource'


def _fix_ns(name):
    if name[:1] == '{':
        c = name.index('}')
        return name[1:c] + name[c + 1:]
    return name


def mini_parser(xml):
    root = fromstring(xml)
    objects = []
    for desc in root:
        if desc.tag != RDF_DESC:
            raise ValueError(desc.tag)
        if RDF_ABOUT in desc.attrib:
            subject = _fix_ns(desc.attrib[RDF_ABOUT])
        elif RDF_NODE_ID in desc.attrib:
            subject = _fix_ns(desc.attrib[RDF_NODE_ID])
        else:
            raise ValueError(desc.attrib)
        for child in desc:
            pred = _fix_ns(child.tag)
            if RDF_RESOURCE in child.attrib:
                val = URI(child.attrib[RDF_RESOURCE])
            elif RDF_NODE_ID in child.attrib:
                # XXX
                #val = URI(child.attrib[RDF_NODE_ID])
                continue
            else:
                val = child.text
            objects.append((subject, pred, val))
    return objects


def objects(rdfs):
    ret = []
    for rdf in rdfs:
        if hasattr(rdf, 'decode'):
            rdf = rdf.decode('utf8')
        ret.extend(mini_parser(rdf))
    return ret


def deconstruct(graphs):
    subjects = {}
    subjects_by_type = {}
    # TODO: deal with merging 'owl#sameAs'
    for graph in graphs:
        graph = mini_parser(graph)
        for subject, pred, obj in graph:
            subject = shorten_name(subject)
            pred = shorten_name(pred)
            if isinstance(obj, URI):
                obj = shorten_name(obj)
            else:
                obj = obj
            if pred == 'rdf#type':
                subjects_by_type.setdefault(obj, []).append(subject)
            p = subjects.setdefault(subject, {}).setdefault(pred, [])
            if obj not in p:
                p.append(obj)
    return subjects, subjects_by_type
