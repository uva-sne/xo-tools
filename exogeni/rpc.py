from __future__ import print_function
from sys import stderr
from ssl import SSLContext, PROTOCOL_TLSv1_2
from xmlrpc.client import ServerProxy
from datetime import datetime, timedelta
from re import compile

from exogeni import _parse_rdf

# TODO: properly parse output


class ResponseError(ValueError):
    pass

r_reservation_id = compile(r'\[  '
                           r'Reservation UID: ([0-9a-fA-F\-]+) \| '
                           r'Resource Type: ([a-zA-Z0-9\-\.]+) \| '
                           r'Units: (\d+) \| '
                           r'Status: ([A-Za-z\- ]+) '
                           r'\]')

# lease_plan = ('Here are the leases: \n'
#               'Request id: 9486c88c-3e2f-436c-8864-29189e128712\n'
#               '[   Slice UID: 9486c88c-3e2f-436c-8864-29189e128712 | '
#               'Reservation UID: d0ac03a1-4b40-46ac-b583-acc6f8e11c3b | '
#               'Resource Type: uvanlvmsite.vm | Resource Units: 1 ] \n'
#               'No errors reported')


def build_context(url, cert, key):
    ctx = SSLContext(PROTOCOL_TLSv1_2)
    ctx.load_cert_chain(cert, key)
    if ':' not in url:
        raise ValueError('Invalid controller URL (config missing?)')
    rpc = ServerProxy(url, context=ctx)
    return rpc


def version(rpc):
    return rpc.orca.getVersion()


def _parse_output(resp, parse_func=None):
    err = None
    if resp.get('err') or 'ret' not in resp:
        err = resp.get('err')
        msg = resp.get('msg')
        if isinstance(err, bool) and msg and len(resp) == 2:
            err = msg or 'Unspecified error'
        else:
            # TODO
            raise ValueError(resp)
    ret = resp.get('ret')
    if parse_func and ret is not None:
        ret = parse_func(ret)
    return ret, err


def list_resources(rpc):
    return _parse_output(rpc.orca.listResources([], {}), _parse_rdf)


def create_slice(rpc, urn, req, users):
    return _parse_output(rpc.orca.createSlice(urn, [], req, users))


def modify_slice(rpc, urn, req):
    return _parse_output(rpc.orca.modifySlice(urn, [], req))


def slice_status(rpc, urn):
    """<RDF dump>"""
    return _parse_output(rpc.orca.sliceStatus(urn, []), _parse_rdf)


def slice_reservation_ids(rpc, urn):
    """Get reservation UUIDs.

    Sadly, there's no clean way of doing this."""
    resp, err = _parse_output(rpc.orca.sliceStatus(urn, []))
    if err:
        return resp, err

    lines = resp.split('\n')
    if not lines.pop(0).startswith('ORCA'):
        raise ValueError(resp)
    ids = []
    while len(lines) >= 2:
        if not lines.pop(0).startswith('****'):
            break
        line = lines.pop(0)
        m = r_reservation_id.search(line)
        if not m:
            raise ValueError(resp)
        ids.append(m.groups())
    return (ids, err)


def list_slices(rpc):
    """List of slice URNs"""
    return _parse_output(rpc.orca.listSlices([]))


def delete_slice(rpc, urn):
    """Delete slice"""
    return _parse_output(rpc.orca.deleteSlice(urn, []))


def sliver_properties(rpc, urn, uuid):
    """Get sliver properties (key/value pairs)"""
    return _parse_output(rpc.orca.getSliverProperties(urn, uuid, []))


def renew_slice(rpc, urn, end):
    if isinstance(end, timedelta):
        end = datetime.now() + end

    if isinstance(end, datetime):
        tz = end.strftime('%z')
        if not tz:
            tz = 'Z'
        end = end.strftime('%Y-%m-%dT%H:%M:%S') + tz

    return _parse_output(rpc.orca.renewSlice(urn, [], end))
