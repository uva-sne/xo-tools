"""
NDL-OWL
-------

TODO: how much translation do we really want to do?
"""

if 1:
    from sys import stderr
    from pprint import pprint
    trace = lambda *a: pprint(a[0] if len(a) == 1 else a, stream=stderr)
else:
    trace = lambda *a: None


# Meta types we don't care about
ignore = {
    'owl#Class',
    'owl#DatatypeProperty',
    'owl#ObjectProperty',
    'owl#Ontology',
    'owl#Restriction',
}

ignore_meta = {
    'collections#item',
    'compute#diskImage',
    'domain#hasResourceType',
    'domain#hasService',
    'ec2#hasInstanceID',
    'layer#atLayer',
    'manifest#hasParent',
    'openflow#quantumNetUUID',
    'rdf#label',
    'rdf#type',
    'request#inDomain',
    'request#message',
    'request#postBootScript',
    'topology#hasGUID',
    'topology#hasInterface',
    'topology#hasName',
    'topology#hasURL',
}


def single_value(vs):
    assert len(vs) == 1
    return vs[0]


def kind_from_image(image):
    if image.startswith('img-'):
        image = image[4:]
    if image == 'ovs' or image == 'bridge':
        return 'switch'
    elif 'router' in image or image.endswith('edge'):
        return 'router'
    elif image == 'alpine':
        return 'node'
    return image


def expand_name(k):
    # NB: no owl
    parts = k.split('#', 1)
    return 'http://geni-orca.renci.org/owl/' + parts[0] + '#' + parts[1]


def parse_manifest(subjects, subjects_by_type):
    def element_name(k):
        return k.split('#', 1)[-1]

    def copy_meta(elem):
        return {k: ' '.join(v) for k, v in elem.items()
                if k not in ignore_meta}

    def fixup_slashes(n):
        return n.split('/', 1)[0]

    nodes = {}
    links = {}
    vlans = {}
    stitches = {}
    result = {}
    node_iface_map = {}

    def node_ifaces(node_name, iface_names):
        # For some stupid reason the NDL now contains duplicate interfaces
        #  with a sameAs entry; for now we build a simple mapping; we assume
        #  some naming and element presence convention here that ExoGENI might
        #  change at any random point in time
        iface_table = {}
        for iface in iface_names:
            elem = subjects[iface]
            if iface.endswith('/intf'):
                if 'owl#sameAs' not in elem:
                    raise ValueError(iface)
                iface = single_value(elem['owl#sameAs'])
            elif 'owl#sameAs' in elem:
                raise NotImplementedError(iface)
            data = iface_table.setdefault(iface, {})
            data.update(elem)

        ifaces = []
        for iface, data in iface_table.items():
            iface_name = element_name(iface)
            _iface = {'name': iface_name}
            if 'layer#label_ID' in data:
                _iface['ipv4'] = data['layer#label_ID']
            elif 'ip4#localIPAddress' in data:
                # XXX: requires translating mask
                #s = subjects[single_value(elem['ip4#localIPAddress'])]
                #_iface['ipv4'] = s
                pass
            if 'ip4#macAddress' in data:
                _iface['mac'] = single_value(data['ip4#macAddress'])
            ifaces.append(_iface)
            # The /intf interface is not really used in this case
            node_iface_map[_iface['name']] = _iface
        return ifaces

    def add_node(k, elem):
        # TODO: get rid of "metadata"
        n = {}
        if 'topology#hasName' in elem:
            n['name'] = single_value(elem['topology#hasName'])
        else:
            n['name'] = element_name(k)
        n['metadata'] = copy_meta(elem)
        n['guid'] = single_value(elem['topology#hasGUID'])
        n['unit_url'] = expand_name(k)
        if 'compute#diskImage' in elem:
            n['image'] = element_name(single_value(elem['compute#diskImage']))
            n['kind'] = kind_from_image(n['image'])
        else:
            # TODO
            n['kind'] = 'baremetal'
        n['ifaces'] = node_ifaces(n['name'],
                                  elem.get('topology#hasInterface', []))

        if 'request#inDomain' in elem:
            domain = single_value(elem['request#inDomain'])
            n['site'] = domain.split('vmsite.rdf#', 1)[0]

        if 'domain#hasService' in elem:
            for s in elem['domain#hasService']:
                s = subjects[s]
                if 'topology#managementIP' in s:
                    n['remote_ip'] = single_value(s['topology#managementIP'])
        nodes[k] = n

    def add_link(k, elem):
        n = {}
        n['metadata'] = copy_meta(elem)
        items = elem['collections#item']
        n['source'] = element_name(items[0])
        n['target'] = element_name(items[1])
        n['guid'] = single_value(elem['topology#hasGUID'])
        n['name'] = element_name(k)

        links[k] = n

    def add_vlan(k, elem):
        #  domain#hasResourceType = domain#VLAN
        n = {}
        n['name'] = element_name(k)
        n['guid'] = single_value(elem['topology#hasGUID'])
        n['metadata'] = copy_meta(elem)
        nodes = n['nodes'] = []
        for node in elem.get('collections#item', []):
            nodes.append(element_name(node))
        vlans[k] = n

    def add_stitch_port(k, elem):
        n = {}
        n['name'] = element_name(k)
        n['guid'] = single_value(elem['topology#hasGUID'])
        n['bandwidth'] = single_value(elem['layer#bandwidth'])
        vlans = []
        for iface in elem['topology#hasInterface']:
            iface_elem = subjects[iface]
            # XXX, this is unreliable
            if 'layer#label' in iface_elem:
                label = single_value(iface_elem['layer#label'])
                vlans.extend(subjects[label]['layer#label_ID'])
        n['vlan'] = single_value(vlans)
        # TODO: remove VLAN suffix
        n['port'] = expand_name(single_value([v for v in
                                              elem['topology#hasInterface']
                                              if '.rdf#' in v]))
        stitches[k] = n

    def is_stitch_port(types, elem):
        if 'topology#NetworkConnection' not in types:
            return False
        # This is a bit hacky
        return any('.rdf#' in v
                   for v in elem.get('topology#hasInterface', []))
        #return any(v.endswith('/Stitching/Domain')
        #           for v in elem.get('collections#item', []))

    def resolve_iface(k, elem):
        if 'layer#bandwidth' in elem:
            bw = single_value(link['layer#bandwidth'])
        else:
            bw = None
        for iface_name in elem['topology#hasInterface']:
            iface_elem = element_name(fixup_slashes(iface_name))
            if iface_elem not in node_iface_map:
                #trace('?', iface_elem, node_iface_map.keys())
                #trace(subjects[iface_name])
                continue
            iface = node_iface_map[iface_elem]
            iface.setdefault('links', []).append(link_name)
            if bw:
                iface['bandwidth'] = bw
    # Note that manifests can give an incomplete view when nodes fail
    #manifests = subjects_by_type.get('request#Manifest', reservations)
    reservations = subjects_by_type.get('request#Reservation', [])

    # Slice name:
    for m in reservations:
        reservation = subjects[m]
        if 'topology#hasName' in reservation:
            result['slice_name'] = single_value(reservation['topology#hasName'])
        if 'topology#hasGUID' in reservation:
            result['slice_guid'] = single_value(reservation['topology#hasGUID'])
        break

    for m in reservations:
        manifest = subjects[m]
        #trace('manifest', m)
        #trace(manifest)

        for k in manifest.get('collections#element', []):
            elem = subjects[k]
            types = elem.get('rdf#type', [])
            if 'compute#ComputeElement' in types:
                add_node(k, elem)
            elif 'topology#BroadcastConnection' in types:
                add_vlan(k, elem)
            elif 'topology#LinkConnection' in types:
                add_link(k, elem)
            elif 'topology#BroadcastConnection' in types:
                raise NotImplementedError(elem)
            elif is_stitch_port(types, elem):
                add_stitch_port(k, elem)
            else:
                if types != ['topology#Device']:
                    trace('unknown', k, elem)

    for m in subjects_by_type.get('topology#NetworkConnection', []):
        link = subjects[m]
        link_name = element_name(m)
        resolve_iface(m, link)

    result['nodes'] = {element_name(k): v for k, v in nodes.items()}
    for n in nodes.values():
        ifaces = n.get('ifaces')
        if 'ipv4' in n: n['ipv4'].sort()
        if 'links' in n: n['links'].sort()
        if not ifaces:
            continue
        ifaces.sort(key=lambda k: k['name'])
    result['links'] = {element_name(k): v for k, v in links.items()}
    if vlans:
        result['vlans'] = {element_name(k): v for k, v in vlans.items()}
    if stitches:
        result['stitches'] = {element_name(k): v for k, v in stitches.items()}

    return result
