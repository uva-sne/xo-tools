from uuid import uuid4

def guid():
    return str(uuid4())
