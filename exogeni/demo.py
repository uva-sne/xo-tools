import asyncio 
#from asyncio import async, coroutine, sleep, create_subprocess_exec
from asyncio import coroutine, sleep, create_subprocess_exec
from subprocess import PIPE
from json import loads, dumps

from ..log import create_logger
from ..topology import upsert_rgraph

logging = create_logger('demo')


SLICE_ID = 'ciena-demo-live'

poller_task = None


# Maybe:
# On start, test if slices exist

@coroutine
def clean_slice():
    logging.info('Cleaning running topology')
    proc = yield from create_subprocess_exec('orca-rm', SLICE_ID)
    ret = yield from proc.wait()
    global poller_task
    if poller_task:
        poller_task.cancel()
        poller_task = None
    return ret



@coroutine
def submit_topology_request(graph):
    # <validate and transform to NDL>
    # <launch request>
    logging.info('Submitting request to ORCA')
    proc = yield from create_subprocess_exec('vectors15-submit', SLICE_ID, stdin=PIPE, stderr=PIPE)
    body = dumps(graph).encode('utf8')
    _, err = yield from proc.communicate(body)
    ret = yield from proc.wait()
    if ret:
        if err:
            err = err.decode('utf8').rstrip()
        raise ValueError('Failed to submit request to ORCA: %s (%s)' % (err, ret,))

    # <launch poll_slice>
    global poller_task
    if poller_task is not None:
        try:
            poller_task.cancel()
        except:
            logging.exception('Poll task fucked up')

    logging.info('Launching poll task')
    #poller_task = async(poll_slice())
    poller_task = asyncio.run(poll_slice())


@coroutine
def poll_slice():
    cur_graph = None
    yield from sleep(2)
    while True:
        try:
            slice_graph = yield from get_slice_graph()
            if not cur_graph or slice_graph != cur_graph:
                yield from upsert_rgraph('virtual0', slice_graph)
            cur_graph = slice_graph
        except:
            logging.exception('Failed to get an updated slice graph')
            # <notify>

        yield from sleep(30)


@coroutine
def get_slice_graph():
    proc = yield from create_subprocess_exec('orca-poll', SLICE_ID, stdout=PIPE)
    graph, _ = yield from proc.communicate()
    ret = yield from proc.wait()
    if ret != 0:
        raise ValueError('Error polling ORCA (%s)' % (ret,))
    graph = loads(graph.decode('utf8'))
    graph['name'] = 'virtual0'
    graph['description'] = 'Temporary'
    graph['parent'] = 'physical0'
    return graph
