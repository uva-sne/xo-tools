from configparser import ConfigParser
from os import environ
from os.path import isdir, join

config = ConfigParser()


def search_config(path=None, explicit_config=None):
    if path:
        config_path = path

    elif isdir('geni'):
        # Deprecated; to be removed.
        config_path = 'geni'

    else:
        config_home = environ.get('XDG_CONFIG_HOME')
        if not config_home:
            config_home = join(environ['HOME'], '.config')
        config_path = join(config_home, 'exogeni')

        if not isdir(config_path):
            # Deprecated; to be removed.
            config_path_old = join(config_home, 'geni')
            if isdir(config_path_old):
                config_path = config_path_old

    if explicit_config:
        config.read(explicit_config)
    else:
        config.read(join(config_path, 'xo-request.ini'))

    return config_path
