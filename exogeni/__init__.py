"""
ExoGENI controller RPC
----------------------
"""

from os import listdir
from os.path import join


def _parse_rdf(buf):
    """
    Parse weird API output.
    """
    lines = buf.split('\n')
    i = 0

    rdfs = []

    while i < len(lines):
        while i < len(lines):
            if lines[i].startswith('<rdf:RDF'):
                break
            #print('Skipping', repr(lines[i]))
            i += 1
        else:
            # No RDF start found
            break
        rdf = ''
        while i < len(lines):
            rdf += lines[i] + '\n'
            if lines[i].startswith('</rdf:RDF'):
                break
            i += 1
        else:
            raise ValueError('No RDF end found')
        i += 1
        rdfs.append(rdf)
    return rdfs


def default_users(config_path):
    # Add all pubkeys from the config directory
    pubkeys = []
    for f in listdir(config_path):
        #if f.startswith('ssh') and f.endswith('.pub'):
        if f.endswith('.pub'):
            pubkeys.append(open(join(config_path, f)).read())
    assert pubkeys
    return [{'login': 'root', 'keys': pubkeys}]
