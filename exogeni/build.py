from hashlib import sha1
from urllib.request import urlopen
from urllib.parse import quote

from xml.sax.saxutils import escape

from . utils import guid

_digest_cache = {}

BANDWIDTH = '100''000''000'


class Literal(str):
    pass


class LocalNode(str):
    pass


OWL_BASE = 'http://geni-orca.renci.org/owl/'

RDF_HEADER = '''<rdf:RDF
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:time="http://www.w3.org/2006/time#"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:app-color="http://geni-orca.renci.org/owl/app-color.owl#"
    xmlns:collections="http://geni-orca.renci.org/owl/collections.owl#"
    xmlns:compute="http://geni-orca.renci.org/owl/compute.owl#"
    xmlns:domain="http://geni-orca.renci.org/owl/domain.owl#"
    xmlns:ethernet="http://geni-orca.renci.org/owl/ethernet.owl#"
    xmlns:geni="http://geni-orca.renci.org/owl/geni.owl#"
    xmlns:ip4="http://geni-orca.renci.org/owl/ip4.owl#"
    xmlns:layer="http://geni-orca.renci.org/owl/layer.owl#"
    xmlns:modify-schema="http://geni-orca.renci.org/owl/modify.owl#"
    xmlns:openflow="http://geni-orca.renci.org/owl/openflow.owl#"
    xmlns:orca="http://geni-orca.renci.org/owl/orca.owl#"
    xmlns:topology="http://geni-orca.renci.org/owl/topology.owl#"
    xmlns:request="http://geni-orca.renci.org/owl/request.owl#"%s>'''

node_sizes = {
    'small': 'XOSmall',
    'medium': 'XOMedium',
    'large': 'XOLarge',
    'xlarge': 'XOXlarge',
    'baremetal': 'ExoGENI-M4'
}


def tag2url(ns, v):
    parts = v.split(':', 1)
    if parts[0] in ns:
        return ns[parts[0]] + quote(parts[1])
    return '%s%s.owl#%s' % (OWL_BASE, parts[0], quote(parts[1]))


def elements_to_rdf(namespaces, elements, default_namespace='local',
                    local_guid=None):
    if default_namespace not in namespaces:
        if not local_guid:
            local_guid = guid()
        namespaces[default_namespace] = OWL_BASE + local_guid + '#'
    spaces = ''
    for k, ns in namespaces.items():
        spaces += '\n    xmlns:%s="%s"' % (k, ns)

    rdf = RDF_HEADER % (spaces,)
    for name, tags in sorted(elements.items()):
        parts = name.split(':', 1)
        if isinstance(name, LocalNode):
            rdf += '\n  <rdf:Description rdf:nodeID="%s">' % (quote(name),)
        elif name.startswith('http:'):
            # XXX
            rdf += '\n  <rdf:Description rdf:about="%s">' % (name,)
        elif parts[0] in namespaces:
            ns = namespaces[parts[0]]
            rdf += '\n  <rdf:Description rdf:about="%s%s">' % (ns, quote(parts[1]))
        elif len(parts) == 2:
            rdf += '\n  <rdf:Description rdf:about="http://geni-orca.renci.org/owl/%s.rdf#%s">' % (quote(parts[0]), quote(parts[1]))
        else:
            raise ValueError(name, tags)
        for k, vs in sorted(tags.items()):
            if not isinstance(vs, (tuple, list)):
                vs = (vs,)
            for v in vs:
                rdf += '\n    '
                # TODO! Buyer beware.
                if isinstance(v, LocalNode):
                    rdf += '<%s rdf:nodeID="%s"/>' % (k, quote(v))
                elif not isinstance(v, Literal) and ':' in v:
                    if v.startswith('http'):
                        res = v
                    else:
                        res = tag2url(namespaces, v)
                    rdf += '<%s rdf:resource="%s"/>' % (k, res)
                else:
                    rdf += '<%s>%s</%s>' % (k, escape(v), k)
        rdf += '\n  </rdf:Description>'
    return rdf + '\n</rdf:RDF>'


def build_image(image, url, cache):
    if url not in cache:
        u = urlopen(url)
        buf = b''
        while True:
            tmp = u.read(5012)
            if tmp == b'':
                break
            buf += tmp
        digest = sha1(buf).hexdigest()
        cache[url] = digest
    else:
        digest = cache[url]

    return 'img-' + image, {'rdf:type': 'compute:DiskImage',
                            'topology:hasURL': Literal(url),
                            'topology:hasGUID': digest}


def element_adder(elements, inventory):
    def add_element(name, tags, inventorize=False):
        if not isinstance(name, LocalNode):
            if ':' not in name:
                name = 'local:' + name
            if inventorize:
                inventory.append(name)
        if name in elements:
            raise KeyError(name)
        elements[name] = tags
        return tags
    return add_element


def build_node(node, add_element=None):
    kind = node['kind']
    if kind == 'baremetal':
        # Special, of course, of course!
        size = kind
        image = None
    else:
        size = node.get('size', 'small')
        image = node.get('image')
        if not image:
            image = node['image'] = kind

    tags = {'rdf:type': 'compute:ComputeElement',
            'request:inDomain': node['domain'],
            'compute:specificCE': 'exogeni:' + node_sizes[size],
            'topology:hasGUID': node.get('guid') or guid(),
            'topology:hasName': node['name'],
            'topology:hasInterface': []}
    if kind == 'baremetal':
        tags['domain:hasResourceType'] = 'compute:BareMetalCE'
    else:
        tags['domain:hasResourceType'] = 'compute:VM'

    if image:
        tags['compute:diskImage'] = 'local:img-' + image

    metadata = node.get('metadata', {})
    if add_element and metadata:
        key = LocalNode('metadata-' + node['name'])
        keys = []
        for k, v in metadata.items():
            attr = LocalNode(key + '-' + k)
            keys.append(attr)
            add_element(attr, {
                'app-color:hasColorKey': Literal(k),
                'app-color:hasColorValue': Literal(v),
                'rdf:type': 'app-color:ColorAttribute'
            })
        add_element(key, {'app-color:hasColorAttribute': keys,
                          'app-color:hasColorLabel': 'VNET',
                          'rdf:type': 'app-color:Color'})
        tags['app-color:hasColor'] = key

    return tags

# BROADCAST_CON = {'rdf:type': 'topology:BroadcastConnection',
#                  'layer:atLayer': 'ethernet:EthernetNetworkElement',
#                  'layer:bandwidth': BANDWIDTH,
#                  'layer:label_ID': '...'}


def resolve_domains(nodes, config, default_site):
    for v in nodes:
        if 'group' not in v:
            v['group'] = default_site
        if 'domain' not in v:
            v['domain'] = config.get('domains', v['group'])


def resolve_images(elements, config, cache=_digest_cache):
    images = set()
    for e in elements.values():
        img = e.get('compute:diskImage')
        if img and img.startswith('local:img-'):
            images.add(img[10:])
    inventory = elements['local:']['collections:element']
    add_element = element_adder(elements, inventory)
    for img in images:
        add_element(*build_image(img, config.get('images', img), cache))


def as_list(v):
    if v is None:
        return []
    elif isinstance(v, dict):
        return list(v.values())
    return v


def graph_to_elements(graph, slice_name):
    """
    Create RDF elements from a graph
    """
    nodes = as_list(graph.get('nodes'))
    links = as_list(graph.get('links'))
    stitches = graph.get('stitches') or []

    elements = {}
    inventory = []
    add_element = element_adder(elements, inventory)

    reservation = elements['local:'] = {
        'rdf:type': 'request:Reservation',
        'request:hasTerm': 'local:Term',
        'collections:element': inventory
    }

    elements['local:TermDuration'] = {
        'time:days': '14',
        'rdf:type': 'http://www.w3.org/2006/time#DurationDescription'}

    elements['local:Term'] = {
        'time:hasDurationDescription': 'local:TermDuration',
        'rdf:type': 'http://www.w3.org/2006/time#Interval'}

    for v in nodes:
        tags = build_node(v, add_element)
        add_element(v['name'], tags, True)

    def add_interface(name, addr, netmask):
        if not name.startswith('local:'):
            name = 'local:' + name
        props = {'rdf:type': 'topology:Interface'}
        if addr:
            v4 = name + '-ip-' + addr.replace('.', '-')
            add_element(v4, {'rdf:type': 'ip4:IPAddress',
                             'ip4:netmask': netmask,
                             'layer:label_ID': addr})
            props['ip4:localIPAddress'] = v4
        add_element(name, props)

    for v in links:
        # Point-to-point links
        src_iface = Literal(v['source'] + '-' + v['name'])
        dst_iface = Literal(v['target'] + '-' + v['name'])
        bandwidth = v.get('bandwidth', BANDWIDTH)

        add_element('local:' + v['name'],
                    {'rdf:type': 'topology:NetworkConnection',
                     'topology:hasInterface': ['local:' + src_iface,
                                               'local:' + dst_iface],
                     'layer:atLayer': 'ethernet:EthernetNetworkElement',
                     'layer:bandwidth': bandwidth,
                     'topology:hasGUID': guid()}, True)

        add_interface(src_iface, v.get('source_v4'), v.get('netmask_v4'))
        add_interface(dst_iface, v.get('target_v4'), v.get('netmask_v4'))

        elements['local:' + v['source']]['topology:hasInterface'] \
            .append('local:' + src_iface)
        elements['local:' + v['target']]['topology:hasInterface'] \
            .append('local:' + dst_iface)

    if graph.get('vlan'):
        raise ValueError('Not supported')

    for i, vlan in enumerate(graph.get('vlans', [])):
        name = vlan.get('name', 'VLAN%s' % (i,))
        bandwidth = vlan.get('bandwidth', BANDWIDTH)
        ifaces = []
        add_element('local:%s' % (name,), {
            'rdf:type': 'topology:BroadcastConnection',
            'layer:bandwidth': bandwidth,
            'layer:atLayer': 'ethernet:EthernetNetworkElement',
            'topology:hasInterface': ifaces,
            'topology:hasGUID': guid()
        }, True)
        for n in vlan['nodes']:
            iface = '%s-%s' % (name, n['name'])
            add_interface(iface, n['ip'], vlan['netmask'])
            elements['local:' + n['name']]['topology:hasInterface'] \
                .append('local:' + iface)
            ifaces.append('local:' + iface)

    for stitch in stitches:
        # URL to switch interface
        whowhere = stitch['iface'].split('#')[0].split('/')[-1].split('.')[0]
        if 'name' in stitch:
            name = stitch['name']
        else:
            name = '%s-%s-%s' % (stitch['node'], whowhere, stitch['vlan'])

        link = 'local:link-' + name
        sid = 'local:stitch-' + name
        vlan = '%s/%s' % (stitch['iface'], stitch['vlan'])
        label = 'local:vlan-label-%s' % (stitch['vlan'],)

        # Stitch
        elem = {'topology:hasInterface': vlan,  # stitch['iface'],
                'topology:hasGUID': guid(),
                'request:inDomain': 'http://geni-orca.renci.org/owl/orca.rdf#Stitching/Domain',
                'rdf:type': 'topology:Device'}
        if 'domain' in stitch:
            elem['topology:hasName'] = stitch['domain']
        add_element(sid, elem, True)

        # Interface
        if stitch['iface'] not in elements:
            add_element(stitch['iface'],
                        {'ethernet:Tagged-Ethernet': [vlan],
                         'rdf:type': 'topology:Interface'})
        elif vlan not in elements[stitch['iface']]['ethernet:Tagged-Ethernet']:
            elements[stitch['iface']]['ethernet:Tagged-Ethernet'].append(vlan)

        # VLAN
        add_element(vlan,
                    {'layer:label': label,
                     'rdf:type': 'topology:Interface'})
        # VLAN label
        if label not in elements:
            add_element(label, {'layer:label_ID': str(stitch['vlan']),
                                'rdf:type': 'layer:Label'})

        # Add NetworkConnection to VM
        bandwidth = stitch.get('bandwidth', BANDWIDTH)
        add_element(link,
                    {'rdf:type': 'topology:NetworkConnection',
                     'topology:hasInterface': [vlan],
                     'layer:atLayer': 'ethernet:EthernetNetworkElement',
                     'layer:bandwidth': bandwidth,
                     'topology:hasGUID': guid()}, True)

        nodes = [stitch['node']] if 'node' in stitch else stitch['nodes']
        for node in nodes:
            iface = 'local:%s-stitch-%s-%s' % (node['name'], whowhere, stitch['vlan'])
            elements[link]['topology:hasInterface'].append(iface)

            # Add interface to VM
            add_interface(iface, node.get('ipv4'), node.get('netmask_v4'))
            elements['local:' + node['name']]['topology:hasInterface'] \
                .append(iface)

    if 'openflow' in graph:
        of = graph.get('openflow')
        elements['local:openflow/controller'] = {
            'topology:hasURL': Literal(of['url']),
            'rdf:type': 'openflow:OFController'
        }
        reservation['geni:slice'] = 'local:of-slice'
        reservation['openflow:openflowCapable'] = 'openflow:OpenFlow-1.0'
        elements['local:of-slice'] = {
            'rdf:type': 'openflow:OFSlice',
            'openflow:controller': 'local:openflow/controller',
            'openflow:hasSlicePassword': Literal(of.get('password', 'of')),
            'topology:hasEmail': Literal(of.get('email', 'undefined'))
        }

    return elements
