
from __future__ import print_function
from time import sleep
from datetime import datetime
from sys import stdout

from .rpc import slice_status
from .rdf import objects, _short_name


MESSAGE = 'http://geni-orca.renci.org/owl/request.owl#message'
RSTATE = 'http://geni-orca.renci.org/owl/request.owl#hasReservationState'


def slice_status_summary(rdfs):
    res = {}

    for subj, term, val in objects(rdfs):
        k = str(subj)
        if k.startswith('http://geni-orca.renci.org/owl/'):
            k = k[31:]
        r = res.get(k, (None, None))
        if str(term) == MESSAGE:
            res[k] = (r[0], str(val).rstrip())
        elif str(term) == RSTATE:
            s = (str(val) or 'unknown').rsplit('#', 1)[-1].lower()
            res[k] = (s, r[1])

    return res


def ewait(rpc, urn):
    start = datetime.now()
    prev = {}

    while True:
        rdfs, err = slice_status(rpc, urn)
        if err:
            raise ValueError(err)
        res = slice_status_summary(rdfs)
        loop = datetime.now()
        ok = True
        blank = False
        for n, v in res.items():
            s = v[0]
            msg = '%s %s' % (s, v[1])
            ps = prev.get(n)
            if ps != s:
                blank = True
                if s == 'Active':
                    print(_short_name(n).ljust(20), msg, loop - start)
                else:
                    print(_short_name(n).ljust(20), msg)
            prev[n] = s
            if s in ('nascent', 'ticketed', 'activeticketed'):
                ok = False
        if ok:
            break
        sleep(5)
        if blank:
            print()
            stdout.flush()

    end = datetime.now()
    print(urn, 'OK', end - start)
